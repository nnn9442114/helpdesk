from django.contrib import admin

from Pages.models import Change, Department, Employee, Order, Place, Priority, Status, Theme, Client


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('fname', 'name', 'patronymic', 'email')
    ordering = ('fname',)
    search_fields = ('fname', 'email')


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('efname', 'ename', 'epatronymic',
                    'place', 'department', 'user')
    ordering = ('efname',)
    search_fields = ('fname', 'place', 'department')


@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)
    ordering = ('title',)


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)
    ordering = ('title',)


@admin.register(Priority)
class PriorityAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)
    ordering = ('title',)


@admin.register(Theme)
class ThemeAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)
    ordering = ('title',)


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)
    ordering = ('title',)


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'theme', 'priority', 'client',
                    'phone', 'emp_executor', 'emp_creator', 'otext', 'cdate', 'edate', 'status', 'resolution')
    search_fields = ('id', 'title')
    list_filter = ('priority', )
    ordering = ('cdate',)


@admin.register(Change)
class ChangeAdmin(admin.ModelAdmin):
    list_display = ('order', 'ctext', 'cdate')
    search_fields = ('ctext',)
    ordering = ('cdate',)
