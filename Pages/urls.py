from django.urls import path
from Pages import views

urlpatterns = [
    path('', views.home, name="home"),
    path('manager/', views.manager, name='manager'),
    path('add_order/', views.add_order, name='add_order'),
    path('order_info/<o_id>', views.order_info, name='order_info'),
    path('find_order/<mode>', views.find_order, name='find_order'),
    path('change_order/<o_id>', views.change_order, name='change_order'),
    path('new_change/<o_id>', views.new_change, name='new_change'),
    path('reports/', views.reports, name='reports'),  
    path('choose_period/', views.choose_period, name="choose_period") , 
]
