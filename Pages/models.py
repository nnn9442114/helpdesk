from django.db import models
from django.contrib.auth.models import User


class Client(models.Model):
    fname = models.CharField('Фамилия', max_length=80)
    name = models.CharField('Имя', max_length=80)
    patronymic = models.CharField('Отчество', max_length=80, null=True)
    email = models.CharField('Почта', max_length=80, null=True)

    def __str__(self):
        return self.fname+' '+self.name+' '+self.patronymic

    class Meta:
        verbose_name = ('Клиент')
        verbose_name_plural = ('Клиенты')


class Place(models.Model):
    title = models.CharField('Название', max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = ('Должность')
        verbose_name_plural = ('Должности')


class Department(models.Model):
    title = models.CharField('Название', max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = ('Подразделение')
        verbose_name_plural = ('Подразделения')


class Employee(models.Model):
    efname = models.CharField('Фамилия', max_length=80)
    ename = models.CharField('Имя', max_length=80)
    epatronymic = models.CharField('Отчество', max_length=80, null=True)
    place = models.ForeignKey(
        Place, verbose_name='Должность', on_delete=models.DO_NOTHING)
    department = models.ForeignKey(
        Department, verbose_name='Подразделение', on_delete=models.DO_NOTHING)
    user = models.OneToOneField(
        User, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.efname+' '+self.ename+' '+self.epatronymic

    class Meta:
        verbose_name = ('Сотрудник')
        verbose_name_plural = ('Сотрудники')


class Priority(models.Model):
    title = models.CharField('Название', max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = ('Приоритет')
        verbose_name_plural = ('Приоритеты')


class Theme(models.Model):
    title = models.CharField('Название', max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = ('Тема')
        verbose_name_plural = ('Темы')


class Status(models.Model):
    title = models.CharField('Название', max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = ('Статус')
        verbose_name_plural = ('Статусы')


class Order(models.Model):
    title = models.CharField('Название', max_length=255)
    theme = models.ForeignKey(
        Theme, verbose_name='Тема', on_delete=models.DO_NOTHING)
    priority = models.ForeignKey(
        Priority, verbose_name='Приоритет', on_delete=models.DO_NOTHING)
    client = models.ForeignKey(
        Client, verbose_name='Клиент', on_delete=models.DO_NOTHING)
    phone = models.CharField('Телефон', max_length=25)
    emp_executor = models.ForeignKey(
        Employee, verbose_name='Наблюдатель', null=True, on_delete=models.DO_NOTHING)
    emp_creator = models.ForeignKey(
        Employee, verbose_name='Создатель', related_name='emp_creator_fk', null=True, on_delete=models.DO_NOTHING)
    otext = models.TextField('Текст', max_length=7000)
    cdate = models.DateField('Дата создания')
    edate = models.DateField('Дата закрытия', null=True)
    status = models.ForeignKey(
        Status, verbose_name='Статус', on_delete=models.DO_NOTHING)
    resolution = models.BooleanField('Резолюция', null=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = ('Заявка')
        verbose_name_plural = ('Заявки')


class Change(models.Model):
    order = models.ForeignKey(
        Order, verbose_name='Заявка', on_delete=models.DO_NOTHING)
    ctext = models.TextField('Изменение', max_length=7000)
    cdate = models.DateField('Дата изменения')

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = ('Изменение')
        verbose_name_plural = ('Изменения')
