import datetime
from django.shortcuts import render
from django.contrib import messages
from Pages.models import Change, Employee, Order, Priority, Status, Theme, Client


def home(request):
    return render(request, 'pages/home.html', {})


def manager(request):
    return render(request, 'pages/manager.html', {})


def add_order(request):
    emp_cr = Employee.objects.filter(user_id=request.user.id)[0]
    themes = Theme.objects.all()
    clients = Client.objects.all()
    priorities = Priority.objects.all()
    emps = Employee.objects.all()

    if request.method == "POST":

        theme_id = request.POST['theme']
        client_id = request.POST['client']
        priority_id = request.POST['priority']
        phone = request.POST['phone']
        title = request.POST['title']
        emp_id = request.POST['emp']
        text = request.POST['text']

        theme = Theme.objects.get(pk=theme_id)
        client = Client.objects.get(pk=client_id)
        priority = Priority.objects.get(pk=priority_id)
        emp = Employee.objects.get(pk=emp_id)
        status = Status.objects.get(title='Создана')

        order = Order(title=title, theme=theme, priority=priority, client=client,
                      phone=phone, emp_executor=emp, emp_creator=emp_cr, otext=text, cdate=datetime.date.today(), status=status)
        order.save()
        messages.success(request, ("Заявка добавлена"))
        return render(request, 'pages/manager.html', {})

    else:
        return render(request, 'pages/add_order.html', {'themes': themes, 'clients': clients, 'priorities': priorities, 'emps': emps})


def find_order(request, mode):
    emp_cr = Employee.objects.filter(user_id=request.user.id)[0]
    orders = Order.objects.all()
    founded = 0
    all = orders.count
    if mode == 'all':
        orders = Order.objects.all()
        founded = orders.count
    elif mode == 'wrez':
        orders = Order.objects.filter(resolution=False)
        founded = orders.count
    elif mode == 'rez':
        orders = Order.objects.filter(resolution=True)
        founded = orders.count
    elif mode == 'creator':
        orders = Order.objects.filter(emp_creator_id=emp_cr.id)
        founded = orders.count
    elif mode == 'executor':
        orders = Order.objects.filter(emp_executor_id=emp_cr.id)
        founded = orders.count
    else:
        orders = Order.objects.all()
        founded = orders.count
    return render(request, 'pages/find_order.html', {'orders': orders, 'all': all, 'founded': founded})


def order_info(request, o_id):
    order = Order.objects.get(pk=o_id)
    changes = Change.objects.filter(order_id=order.id)
    return render(request, 'pages/order_info.html', {'order': order, 'changes': changes})


def change_order(request, o_id):
    order = Order.objects.get(pk=o_id)
    themes = Theme.objects.all()
    clients = Client.objects.all()
    priorities = Priority.objects.all()
    emps = Employee.objects.all()
    statuses = Status.objects.all()
    if request.method == "POST":

        theme_id = request.POST['theme']
        priority_id = request.POST['priority']
        title = request.POST['title']
        emp_id = request.POST['emp']
        text = request.POST['text']
        status_id = request.POST['status']
        edate = request.POST['edate']

        theme = Theme.objects.get(pk=theme_id)
        priority = Priority.objects.get(pk=priority_id)
        emp = Employee.objects.get(pk=emp_id)
        status = Status.objects.get(pk=status_id)

        order.title = title
        order.theme = theme
        order.priority = priority
        order.emp_executor = emp
        order.otext = text
        order.status = status
        if edate != '':
            order.edate = edate
        order.save()
        messages.success(request, ("Заявка изменена"))
    return render(request, 'pages/change_order.html', {'order': order, 'statuses': statuses, 'themes': themes, 'clients': clients, 'priorities': priorities, 'emps': emps})


def new_change(request, o_id):
    order = Order.objects.get(pk=o_id)
    if request.method == "POST":
        text = request.POST['text']
        change = Change(order=order, ctext=text, cdate=datetime.date.today())
        change.save()
        messages.success(request, ("Итерация добавлена"))
    return render(request, 'pages/new_change.html', {'order': order})


def reports(request):
    return render(request, 'pages/reports.html', {})


def choose_period(request):
    if request.method == "POST":
        sd = request.POST['sp']
        ed = request.POST['ep']
        _sd = datetime.datetime.strptime(sd, "%Y-%m-%d").date()
        _ed = datetime.datetime.strptime(ed, "%Y-%m-%d").date()
        if _ed < _sd:
            messages.success(request, ("Диапазон некорректен!"))
            return render(request, 'pages/choose_period.html', {})
        else:
            request.session['sd'] = str(_sd)
            request.session['ed'] = str(_ed)
            messages.success(request, ("Диапазон задан."))
            return render(request, 'pages/choose_period.html', {})
    return render(request, 'pages/choose_period.html', {})
